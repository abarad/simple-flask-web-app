from flask import Blueprint, render_template
from flask_login import login_required

from models import User

main = Blueprint('main', __name__)


@main.route('/')
def index():
    return render_template('index.html')


@main.route('/users')
@login_required
def users():
    return render_template('users.html', users=User.query.all())
