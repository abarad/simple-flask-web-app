from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def create_app():
    app = Flask(__name__)

    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'  # TODO: Extract to .env file
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SECRET_KEY'] = 'mysecretkey'  # TODO: Extract to .env file

    db.init_app(app)

    with app.app_context():
        db.create_all()

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    import models

    @login_manager.user_loader
    def load_user(user_id):
        return models.User.query.get(user_id)

    from auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    return app


if __name__ == '__main__':

    app = create_app()

    @app.before_first_request
    def init_database():
        from app import db, create_app
        db.create_all(app=create_app())

    app.run(host='localhost', port=8000, debug=True)
